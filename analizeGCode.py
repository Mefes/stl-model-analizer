import re
import os
import sys
from point import *

dirToModel = "/Volumes/datahd/cube.gcode"

# dirpath = os.getcwd()
# modelname = "/Volumes/datahd/cube.gcode"
# dirToModel = dirpath+modelname

# Prepare file
def prepapreGFile(filedir):
    gf=[]
    gfile = open(filedir)
    for gcode in gfile:
        dictionary={}
        if not gcode.startswith(';'):
            list = gcode.split(' ')
            for command in list:
                command = re.sub('\s+', '', command)
                if 'G' in command:
                    dictionary["command"] = command
                if 'M' in command:
                    dictionary["command"] = command
                if 'X' in command:
                    dictionary["axisX"] = re.sub('X+','', command)
                if 'Y' in command:
                    dictionary["axisY"] = re.sub('Y+','', command) 
                if 'Z' in command:
                    dictionary["axisZ"] = re.sub('Z+','', command)
                if 'F' in command:
                    dictionary["F"] = re.sub('F+','', command)
                if 'E' in command:
                    dictionary["E"] = re.sub('E+','', command)
                if 'S' in command:
                    dictionary["S"] = re.sub('S+','', command)
            if bool(dictionary):
                gf.append(dictionary)
    return gf 

def getListValueAxis(gListOfDict,key):
    gListValue =[]
    for dictionary in gListOfDict:
        if dictionary['command']=='G1':
            if key in dictionary.keys():
                value = float(dictionary[key])
                gListValue.append(value)
            pass
        pass
    pass
    return gListValue

def getModelVolume(gListOfDict):
    glistX=getListValueAxis(gListOfDict,'axisX')
    glistY=getListValueAxis(gListOfDict,'axisY')
    glistZ=getListValueAxis(gListOfDict,'axisZ')
    # мм^3
    return (max(glistX)-min(glistX))*(max(glistY)-min(glistY))*(max(glistZ)-min(glistZ))

def getCoordinateVerification(coordinateList,allowedMaxPoint,allowedMinPoint=0):
    maxPoint = max(coordinateList)
    minPoint = min(coordinateList)
    return False if (minPoint < allowedMinPoint or maxPoint > allowedMaxPoint) else True

def getListofPoints(listXCoordinate,listYCoordinate):
    listOfPoints=[]
    i=0
    while i<len(listXCoordinate):
        listOfPoints.append(Point(listXCoordinate[i],listXCoordinate[i]))
        i+=1
        pass
    # print(listOfPoints)
    return listOfPoints
    
def optimazeCircle(listOfPoints):

    pass

def optmazePoint(listOfPoints):

    pass

def calcCircle(listOfPoints):
    arclist =[]
    
    for point in listOfPoints:
        pass

    pass
# Test
lists = prepapreGFile(dirToModel)
getModelVolume(lists)
response = getCoordinateVerification(getListValueAxis(lists,"axisX"),200)
listX=getListValueAxis(lists,"axisX")
listY=getListValueAxis(lists,"axisY")
getListofPoints(listX,listY)
print(len(listX))
print(len(listY))
print(response)