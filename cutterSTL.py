"""

"""
import vtk 
import math
import os
import sys

import numpy
import stl
from stl import mesh
# Open model

def openModel(filepath):
    reader = vtk.vtkSTLReader()
    reader.SetFileName(filepath)
    return reader

def getMaxMin(model,param):
    minimum = maximum = None
    for p in model.points:
        if minimum is None:
            minimum = p[param]
            maximum = p[param]
        else:
            maximum = max(p[param], maximum)
            minimum = min(p[param], minimum)
    return maximum, minimum

def saveModel(model,filepath):
    stlWriter = vtk.vtkSTLWriter()
    stlWriter.SetFileName(filepath)
    stlWriter.SetInputConnection(model.GetOutputPort())
    stlWriter.Write()
    pass

def setCutterPlane(x,y,z,normX,normY,normZ):
    plane=vtk.vtkPlane()
    plane.SetOrigin(x,y,z)
    plane.SetNormal(normX,normY,normZ)
    return plane

def cut(model,planes):
    stripper = vtk.vtkStripper()
    stripper.SetInputConnection(model.GetOutputPort())
    clipper = vtk.vtkClipClosedSurface()
    clipper.SetInputConnection(stripper.GetOutputPort())
    clipper.SetClippingPlanes(planes)
    return clipper

def partingModel(model,count,maximum,minimum,typeCutting,step = 0):
    i=0
    dim = maximum - minimum
    if step == 0:
        step = dim/count
        print(step)
    elif step < 0:
        raise ValueError("Step can't be the negative value")
    if step < dim/count:
        raise ValueError("Step set less than a minimum step for this count")
    if step > dim:
        raise ValueError("Step set more than a dimension model")
    if count <= 0:
        raise ValueError("Count can't be the negative value or zero")
    while i < count:
        if typeCutting == 0:#X
            planes = vtk.vtkPlaneCollection()
            planes.AddItem(setCutterPlane(0,minimum+(i*step),0,0,1,0))
            i +=1
            planes.AddItem(setCutterPlane(0,minimum+(i*step),0,0,-1,0))
            pass
        elif typeCutting == 1:#Y
            planes = vtk.vtkPlaneCollection()
            planes.AddItem(setCutterPlane(minimum+(i*step),0,0,1,0,0))
            i +=1
            planes.AddItem(setCutterPlane(minimum+(i*step),0,0,-1,0,0))
            pass
        elif typeCutting == 2:#Z
            planes = vtk.vtkPlaneCollection()
            planes.AddItem(setCutterPlane(0,0,minimum+(i*step),0,0,1))
            i +=1
            planes.AddItem(setCutterPlane(0,0,minimum+(i*step),0,0,-1))
            pass
        # Write the stl file to disk
        saveModel(cut(model,planes),"/Users/mefes/Downloads/test"+str(i)+".stl")
        print(i)
        if checkModel("/Users/mefes/Downloads/test"+str(i)+".stl") != True:
            i=0 
            s = minimum
            minimum = -maximum
            maximum = -s
            pass
    
    pass

def checkModel(filename):
    meshModel = mesh.Mesh.from_file(filename)
    maximum,minimum = getMaxMin(meshModel,stl.Dimension.X)
    if maximum == None and minimum == None:
        # raise IOError("Huston we have a problem")
        return False
    else:
        return True

def analizeModel(filename,workDimX,workDimY,workDimZ):
    model = openModel(filename)
    meshModel = mesh.Mesh.from_file(filename)
    maxX, minX = getMaxMin(meshModel,stl.Dimension.X)
    maxY, minY = getMaxMin(meshModel,stl.Dimension.Y)
    maxZ, minZ = getMaxMin(meshModel,stl.Dimension.Z)
    print( maxX, minX)
    dimX = maxX - minX
    dimY = maxY - minY
    dimZ = maxZ - minZ
    volumeModel = dimX*dimY*dimZ
    workSpace = workDimX*workDimY*workDimZ
    if volumeModel > workSpace:
        pass
    if dimX > workDimX:
        partingModel(model,4,maxX,minX,0)
        pass
    if dimY > workDimY:
        partingModel(model,2,maxY,minY,1)
        pass
    if dimZ > workDimZ:
        partingModel(model,4,maxZ,minZ,2)
        pass
    pass
    
# TEST
filename = '/Users/vladimirkadocnikov/Downloads/monster_pencilpot.stl'
analizeModel(filename,10,100,100)
# model = openModel(filename)
# meshModel = mesh.Mesh.from_file(filename)
# maxZ, minZ = getMaxMin(meshModel,stl.Dimension.Z)
# print(minZ,maxZ)
# parting model without step
# partingModel(model,2,dimZ,minZ,-2)
# parting model with step
# partingModel(model,2,maxZ,minZ)

